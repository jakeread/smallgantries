# Small Gantries

Following the [rctgantries](https://gitlab.cba.mit.edu/jakeread/rctgantries) project, this tiny machine uses NEMA17 size 'RCT Elements' to build out a small two axis machine. 

I developed this mostly for testing out networked stepper control code.

CAD of the machine is in the repo.

![cad](images/smallgantries-cad.png)

![fab](images/smallgantries-fab.jpg)

![video](video/mocontrol-2.mp4)